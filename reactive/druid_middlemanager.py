import math, os, stat
from subprocess import run as sub_run
from psutil import virtual_memory
from charms.reactive import when, when_not, when_any, is_flag_set, set_flag, clear_flag, endpoint_from_flag
from charms.templating.jinja2 import render
from charmhelpers.core.hookenv import resource_get, status_set, log
from subprocess import check_call


@when_not('druid-middlemanager.installed')
def install_druid_middlemanager():
    memory_check = check_sufficient_memory()
    if not memory_check[0]:
        log('Can not start MiddleManager, server has ' + memory_check[1] + 'GB of RAM, '
               'but MiddleManager requires at least ' + memory_check[2] + 'GB')
        status_set('blocked', 'MiddleManager has insufficient RAM '
                              '(has ' + memory_check[1] + 'GB, requires ' + memory_check[2] + 'GB')
    else:
        archive = resource_get("druid")
        if not os.path.isdir('/opt/druid_middlemanager'):
            os.mkdir('/opt/druid_middlemanager')
        cmd = ['tar', 'xfz', archive, '-C', '/opt/druid_middlemanager', '--strip', '1']
        check_call(cmd)

        archive = resource_get("mysql-extension")
        cmd = ['tar', 'xfz', archive, '-C', '/opt/druid_middlemanager/extensions']
        check_call(cmd)

        os.mkdir('/opt/druid_middlemanager/var')
        os.mkdir('/opt/druid_middlemanager/var/tmp')
        os.mkdir('/opt/druid_middlemanager/var/druid')
        os.mkdir('/opt/druid_middlemanager/var/druid/indexing-logs')
        os.mkdir('/opt/druid_middlemanager/var/druid/segments')
        os.mkdir('/opt/druid_middlemanager/var/druid/segment-cache')
        os.mkdir('/opt/druid_middlemanager/var/druid/task')
        os.mkdir('/opt/druid_middlemanager/var/druid/hadoop-tmp')
        os.mkdir('/opt/druid_middlemanager/var/druid/pids')
        os.mkdir('/opt/druid_middlemanager/log')

        render('druid_middlemanager', '/etc/init.d/druid_middlemanager')
        render('druid_logrotate', '/etc/logrotate.d/druid_logrotate')
        st = os.stat('/etc/init.d/druid_middlemanager')
        os.chmod('/etc/init.d/druid_middlemanager', st.st_mode | stat.S_IEXEC)

        set_flag('druid-middlemanager.installed')
        status_set('waiting', 'Waiting for config file')


@when('druid-middlemanager.installed', 'endpoint.config.new_config')
def configure_druid_middlemanager():
    with open('/opt/druid_middlemanager/conf/druid/_common/common.runtime.properties', 'r') as c:
        old_conf = c.read()

    config = endpoint_from_flag('endpoint.config.new_config')
    new_conf = config.get_config()

    if old_conf != new_conf:
        log('New config detected! Resetting Druid Middlemanager...')
        if is_flag_set('druid-middlemanager.configured'):
            clear_flag('druid-middlemanager.configured')

        status_set('maintenance', 'Configuring Middlemanager')

        config_file = open('/opt/druid_middlemanager/conf/druid/_common/common.runtime.properties', 'w')
        config_file.write(new_conf)
        config_file.close()

        write_runtime_properties()
        write_jvm_config()

        set_flag('druid-middlemanager.configured')
        set_flag('druid-middlemanager.new_config')
        status_set('maintenance', 'Druid Middlemanager configured, waiting to start process...')


@when('druid-middlemanager.installed', 'endpoint.config.new_hdfs_files')
@when_not('druid-middlemanager.hdfs_configured')
def configure_hdfs_files():
    hdfs = endpoint_from_flag('endpoint.config.new_hdfs_files')
    new_hdfs_files = hdfs.get_hadoop_files()

    status_set('maintenance', 'Copying HDFS XML Files.')

    with open('/opt/druid_middlemanager/conf/druid/_common/core-site.xml', 'w') as f:
        f.write(new_hdfs_files[0])
    with open('/opt/druid_middlemanager/conf/druid/_common/hdfs-site.xml', 'w') as f:
        f.write(new_hdfs_files[1])
    with open('/opt/druid_middlemanager/conf/druid/_common/mapred-site.xml', 'w') as f:
        f.write(new_hdfs_files[2])
    with open('/opt/druid_middlemanager/conf/druid/_common/yarn-site.xml', 'w') as f:
        f.write(new_hdfs_files[3])

    if is_flag_set('druid-middlemanager.configured'):
        clear_flag('druid-middlemanager.configured')

    set_flag('druid-middlemanager.hdfs_configured')
    set_flag('druid-middlemanager.new_config')
    status_set('waiting', 'HDFS files copied. Waiting...')


@when_any('druid-middlemanager.configured', 'druid-middlemanager.hdfs_configured')
@when('java.ready', 'druid-middlemanager.new_config')
def run_druid_middlemanager(java):
    restart_middlemanager()
    clear_flag('druid-middlemanager.new_config')


def start_middlemanager():
    status_set('maintenance', 'Starting Middlemanager...')
    code = sub_run(['/etc/init.d/druid_middlemanager', 'start'], cwd='/opt/druid_middlemanager')
    if code.returncode == 0:
        set_flag('druid-middlemanager.running')
        status_set('active', 'Middlemanager running')
    else:
        status_set('blocked', 'Error starting Middlemanager')


def stop_middlemanager():
    status_set('maintenance', 'Stopping Middlemanager...')
    code = sub_run(['/etc/init.d/druid_middlemanager', 'stop'], cwd='/opt/druid_middlemanager')
    if code.returncode == 0:
        clear_flag('druid-middlemanager.running')
        status_set('waiting', 'Middlemanager stopped')
    else:
        status_set('blocked', 'Error stopping Middlemanager')


def restart_middlemanager():
    status_set('maintenance', 'Restarting Middlemanager...')
    stop_middlemanager()
    start_middlemanager()


def write_runtime_properties():

    mem_gb = check_system_memory() // (1024 ** 3)  # Total memory of server

    Xmx = math.floor(mem_gb)

    context = {
        'Xmx': Xmx
    }

    render('runtime.properties', '/opt/druid_middlemanager/conf/druid/middleManager/runtime.properties', context)


def write_jvm_config():

    mem_gb = check_system_memory() // (1024**3) # Total memory of server

    Xmx = math.floor(mem_gb)

    context = {
        'Xmx': Xmx
    }

    render('jvm.config', '/opt/druid_middlemanager/conf/druid/middleManager/jvm.config', context)


def check_system_memory():

    mem = virtual_memory()
    return mem.total


def check_sufficient_memory():
    """
    This method checks to make sure there is sufficient memory before allowing the charm to continue installing.
    """

    # Get minimum memory requirement for the heap. Round up since we'll round up when allocating Xms
    # 64 MB, convert to GB
    heap = math.ceil(64 / 1024)

    # Get total memory of the system
    mem = check_system_memory() // (1024 ** 3)

    # If the memory requirements exceed the amount available
    if heap > mem:
        return False, str(mem), str(int(heap))
    else:
        return True, 0, 0
