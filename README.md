# Overview

Welcome to the Druid Middlemanager charm by Spicule. The Middlemanager node is a worker node that 
executes submitted tasks. 

For more information on Druid Middlemanager, please visit the [Druid Middlemanager Documentation](http://druid.io/docs/latest/design/middlemanager.html).

# Usage

To deploy this charm from the command line, enter the following:

    juju deploy cs:~spiculecharms/druid-middlemanager --series xenial

You can specify hardware constraints for your Middlemanager node on the command 
line. To find out more, refer to the ["Using Constraints"](https://docs.jujucharms.com/2.4/en/charms-constraints").
For example, to deploy your Middlemanager node on a server with 4 cores and 15 GB
of RAM, enter the following command:

    juju deploy cs:~spiculecharms/druid-middlemanager --series xenial --constraints "mem=15G cores=4"

# OpenJDK and Druid Config

Druid Middlemanager requires a relation to the [OpenJDK charm](https://jujucharms.com/openjdk/)
to automate the installation of Java onto Middlemanager's server, which is a
requirement of Middlemanager.

Additionally, Middlemanager requires configuration as part of the wider Druid
cluster, so Middlemanager must be related to our [Druid Config charm](https://jujucharms.com/new/u/spiculecharms/druid-config).
The individual Druid nodes do not need to be directly related to one another,
and should instead be directly related to Druid Config instead. 

# Memory Requirements

Supplying the right amount of memory for a MiddleManager node is crucial. Supplying
insufficient memory to Middlemanager will result in the node failing to start up, so
the Middlemanager charm has a built-in test that checks if the system has sufficient 
memory for Middlemanager to function. This equates to a simple formula, where the
result is expressed in Gigabytes of memory:

    (number of cores + number of mergeBuffers + 1) * 1 GB

where number of mergeBuffers relates to number of cores as follows:

*if number of cores is less than 8, number of mergeBuffers = 2
*if number of cores is more than 8, number of mergeBuffers = number of cores / 4
*(note: number of mergeBuffers should be rounded down if number of cores is more than 8)

For example, if we have a 1 core server:

    (1 + 2 + 1) * 1 GB = 4 GB of RAM

If we have a 16 core server:

    (16 + (16/4) + 1) * 1 GB = 21 GB of RAM

Please specify this requirement as a constraint as detailed in the Usage section above.

# Configuration

Druid Cluster configuration is handled through the [Druid Config charm](https://jujucharms.com/new/u/spiculecharms/druid-config).

# Contact Information

Thank you for using the Druid Middlemanager charm - we hope you find it useful! For
more information, here are our contact details:

Spicule: [http://www.spicule.co.uk](http://www.spicule.co.uk)  
Anssr: [http://anssr.io](http://anssr.io)  
Email: info@spicule.co.uk  

For assistance with Juju, please refer to the [Juju Discourse](https://discourse.jujucharms.com/).

## Druid

For more information about Druid, please refer to the [Druid website](http://druid.io).

